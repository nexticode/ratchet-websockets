<?php

require __DIR__ . '/../vendor/autoload.php';

use Rx\Thruway\Client;

$data = array(
    'name' => $_POST['name'],
    'user_id' => $_POST['user_id'],
    'message' => $_POST['message'],
);

$wampClient = new Client('ws://0.0.0.0:9990', 'realm1');

// echo json_encode($wampClient);

$wampClient->publish("notifications_{$data['user_id']}", $data);

//die();
